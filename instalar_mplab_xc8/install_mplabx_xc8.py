import subprocess


def run():
    print('\nDESCARGAR MPLAB X\n')
    url = 'https://bit.ly/3tLSLzy'
    subprocess.run(['powershell.exe', 'wget', '-O', 'mplabx.exe', url])

    print('\nDESCARGAR XC8\n')
    url = 'https://bit.ly/3KwcxFY'
    subprocess.run(['powershell.exe', 'wget', '-O', 'xc8.exe', url])

    print('\nEJECUTAR MPLAB X\n')
    subprocess.run(['mplabx.exe'])

    print('\nEJECUTAR XC8\n')
    subprocess.run(['xc8.exe'])


if __name__ == '__main__':
    run()
