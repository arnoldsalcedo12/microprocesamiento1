#include <stdio.h>


int main(void)
{
    int num1 = 0;
    int num2 = 0;

    int *p_num1, *p_num2, aux = 0;


    printf("\nIngrese el primer número entero: \n");
    scanf("%d",&num1);

    printf("\nIngrese el segundo número entero: \n");
    scanf("%d",&num2);

    printf("\nnum1 = %d y num2 = %d\n", num1, num2);
    printf("\nIntercambiados\n");

    p_num1 = &num1;
    p_num2 = &num2;

    aux = *p_num1;
    *p_num1 = *p_num2;
    *p_num2 = aux;

    printf("\nnum1 = %d y num2 = %d\n\n", num1, num2);


    return 0 ;
}
