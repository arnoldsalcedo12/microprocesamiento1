#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    unsigned int size = 0, i = 0;

    double *vector1;
    double *vector2;
    double resultado = 0.0;


    printf("\nIngrese el tamaño de los vectores: \n");
    scanf("%d", &size);


    vector1 = (double *)malloc(size*sizeof(double));
    vector2 = (double *)malloc(size*sizeof(double));

    printf("\nIngrese los elementos del primer vector: \n");

    for(i=0; i<size; i++)
    {
        printf("vector1[%d]: ", i);
        scanf("%lf", &vector1[i]);
    }

    printf("\nIngrese los elementos del segundo vector: \n");

    for(i=0; i<size; i++)
    {
        printf("vector2[%d]: ", i);
        scanf("%lf", &vector2[i]);
    }

    for(i=0; i<size; i++)
    {
        resultado += vector1[i]*vector2[i];
    }


    printf("\nEl producto escalar de los vectores es: %lf.\n\n", resultado);


    free(vector1);
    free(vector2);

    return 0;
}

