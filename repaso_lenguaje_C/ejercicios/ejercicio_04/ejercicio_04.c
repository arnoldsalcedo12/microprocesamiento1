#include <stdio.h>


float area(float ancho, float largo)
{
    return ancho*largo;
}


int main(void)
{
    float ancho = 0.0;
    float largo = 0.0;
    float resultado = 0.0;

    char unidades;


    printf("Para calcular el área de una habitación primero ingrese la unidad.\n");
    printf("Ingrese p para elegir pies y m para elegir metros: \n");
    scanf("%c", &unidades);

    printf("Ingrese el ancho de una habitación: \n");
    scanf("%f", &ancho);

    printf("Ingrese el largo de una habitación: \n");
    scanf("%f", &largo);


    resultado = area(ancho, largo);


    switch(unidades)
    {
        case 'p':
            printf("El área de la habitación es de %.2f pies cuadrados.\n", resultado);
            break;
        case 'm':
            printf("El área de la habitación es de %.2f metros cuadrados.\n", resultado);
            break;
        default:
            printf("Datos no válidos.\n");
    }


    return 0;
}
