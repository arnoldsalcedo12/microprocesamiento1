#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


int main(void)
{
    unsigned int aux = 0;
    
    TRISB0 = 0; 
    TRISD0 = 1;
    
    RB0 = 0;
    
    while(1)
    {
        if(RD0 == 0)
        {
            __delay_ms(100);
            
            if(RD0 == 0)
            {
                aux = !aux;
                __delay_ms(100);
            }
        }
        
        RB0 = aux;
    }

    return 0;
}
