#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


int main(void)
{
    TRISB0 = 0; //Pone el RB0 como salida.
    
    while(1)
    {
        RB0 = 1; //Encender el LED.
        __delay_ms(1000); //Retardo de un segundo.
        
        RB0 = 0; //Apagar el LED.
        __delay_ms(1000);
    }  

    return 0;
}
