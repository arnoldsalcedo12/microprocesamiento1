#include <xc.h>

#pragma config FOSC = XT 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 4000000


int main(void)
{  
    int duty = 0;
    
    PORTC = 0x00;
    TRISC1 = 0; // Pin RC1(CCP2) como salida.
 
    // Prescaler del TMR2 igual a 1.
    T2CKPS1 = 0;
    T2CKPS0 = 0;
    
    TMR2ON = 1; // Timer 2 encendido.
    
    PR2 = 99;  // Establecer el periodo de PWM por medio de PR2.
    CCP2CON = 0x0C; // CON2M<0:3> = 0b1100; Modo PWM.
    
    duty = 300; // Ciclo de trabajo de 75%
    DC2B1 = duty & 2;
    DC2B0 = duty & 1;
    CCPR2L = duty>>2;
    
    return 0;
}

