#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


int main(void)
{    
    ANSELH = 0x00;
    PORTB = 0x00;    
    TRISB = 0x0F;
    WPUB = 0x0F;
    OPTION_REG = 0x7F;
    IOCB = 0x00;

    while(1)
    {       
        if(RB0 == 0)
        {
            __delay_ms(100);

            if(RB0 == 0)
            {
                RB4 = !RB4;
                __delay_ms(100);
            }
        }
        
        if(RB1 == 0)
        {
            __delay_ms(100);

            if(RB1 == 0)
            {
                RB5 = !RB5;
                __delay_ms(100);
            }
        }
        
        if(RB2 == 0)
        {
            __delay_ms(100);

            if(RB2 == 0)
            {
                RB6 = !RB6;
                __delay_ms(100);
            }
        }
        
        if(RB3 == 0)
        {
            __delay_ms(100);

            if(RB3 == 0)
            {
                RB7 = !RB7;
                __delay_ms(100);
            }
        }
    }

    return 0;
}
