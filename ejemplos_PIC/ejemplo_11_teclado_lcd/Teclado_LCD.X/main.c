#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <xc.h>
#include "lcd.h"

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000

const unsigned char text[17] = {' ', '7', '8', '9', '/',
                                     '4', '5', '6', '*',
                                     '1', '2', '3', '-',
                                     'c', '0', '=', '+'};

unsigned int leer_teclado(void)
{   
    if(PORTB != 0x0F)
    {
        PORTB = 0XE0;
        if(RB3 == 0)
            return 1;
        if(RB2 == 0)
            return 2;
        if(RB1 == 0)
            return 3;
        if(RB0 == 0)
            return 4;
        
        PORTB = 0xD0;
        if(RB3 == 0)
            return 5;
        if(RB2 == 0)
            return 6;
        if(RB1 == 0)
            return 7;
        if(RB0 == 0)
            return 8;
        
        PORTB = 0xB0;
        if(RB3 == 0)
            return 9;
        if(RB2 == 0)
            return 10;
        if(RB1 == 0)
            return 11;
        if(RB0 == 0)
            return 12;
        
        PORTB = 0x70;
        if(RB3 == 0)
            return 13;
        if(RB2 == 0)
            return 14;
        if(RB1 == 0)
            return 15;
        if(RB0 == 0)
            return 16;
    }
    
    return 0;
}

int main ( void )
{
    unsigned int teclado = 0;
    unsigned int selector = 0;
    
    ANSELH = 0x00;
    PORTB = 0x00;    
    TRISB = 0x0F;
    WPUB = 0x0F;
    OPTION_REG = 0x7F;
    IOCB = 0x00;
    
    LCD lcd = { &PORTC, 2, 3, 4, 5, 6, 7 }; // PORT, RS, EN, D4, D5, D6, D7

    LCD_Init(lcd);

    LCD_Clear();
    
    while(1)
    {
        teclado = leer_teclado();
        
        switch(teclado)
        {
            case 1:
                selector = 1;
                break;
            case 2:
                selector = 2;
                break;
            case 3:
                selector = 3;
                break;
            case 4:
                selector = 4;
                break;
            case 5:
                selector = 5;
                break;
            case 6:
                selector = 6;
                break;
            case 7:
                selector = 7;
                break;
            case 8:
                selector = 8;
                break;
            case 9:
                selector = 9;
                break;
            case 10:
                selector = 10;
                break;
            case 11:
                selector = 11;
                break;
            case 12:
                selector = 12;
                break;
            case 13:
                selector = 13;
                break;
            case 14:
                selector = 14;
                break;
            case 15:
                selector = 15;
                break;
            case 16:
                selector = 16;
                break;
        }
        
        LCD_Set_Cursor(0,3);
        LCD_putrs(" Tecla: ");
        
        LCD_Set_Cursor(0,13);
        LCD_putc(text[selector]);
        __delay_ms(100);

	}

    return 0;
}
