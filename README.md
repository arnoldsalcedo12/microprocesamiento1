# Material de apoyo para Microprocesamiento I
- [Blog PIC](https://diegorestrepoleal.blogspot.com/2021/11/microcontroladores-pic16f887.html)
- [Repaso lenguaje C](https://diegorestrepoleal.blogspot.com/2021/11/repaso-del-lenguaje-c.html)
- [Instalación de MPLAB X y XC8](https://diegorestrepoleal.blogspot.com/2022/03/descargar-e-instalar-mplab-x-con-xc8.html)
- [Guía rápida de nano y tmux](https://diegorestrepoleal.blogspot.com/2022/02/guia-rapida-de-nano-y-tmux.html)

# Cómo Grabar un PIC con PICkit 3
- [Ir al Blog](https://minicontroller.blogspot.com/2021/07/como-grabar-un-pic-con-pickit-3.html)
- [Ia al Vídeo](https://www.youtube.com/watch?v=mkGMmyKLE38)

# Data Sheet del PIC16F887
- [Ir al Data Sheet](http://ww1.microchip.com/downloads/en/devicedoc/41291d.pdf)

# Materiales:
#### PIC16F887:
- [sumador.com](https://sumador.com/products/pic16f887?_pos=1&_sid=916d956f9&_ss=r)
- [sigmaelectronica.net](https://www.sigmaelectronica.net/producto/pic16f887-ip/)
- [electronicoscaldas.com](https://www.electronicoscaldas.com/es/microcontroladores-pic/121-microcontrolador-pic-16f887.html?search_query=PIC16F887&results=1)
- [dynamoelectronics.com](https://dynamoelectronics.com/tienda/microcontrolador-pic-16f887/)
- [didacticaselectronicas.com](https://www.didacticaselectronicas.com/index.php/microcontroladores/microchip/microcontrolador-microchip-pic16f887-ip-chips-microcontroladores-micros-mcu-pic-16-de-40-pines-pic16f887-i-p-40-dip-40-dip40-through-hole-microchip-detail)

#### PICKIT 3:
- [sumador.com](https://sumador.com/products/programador-pickit-3-zocalo-cables?_pos=1&_sid=af41c4c28&_ss=r)
- [vistronica.com](https://www.vistronica.com/componentes-activos/microcontroladores-/programador-pickit-3-mplab-detail.html)
- [didacticaselectronicas.com](https://www.didacticaselectronicas.com/index.php/sistemas-de-desarrollo/microchip2016-02-07-20-36-39_/programador-para-pics-compatible-pickit3-pickit3-in-circuit-debugger-de-microchip-271-programadores-depurador-depuradores-debugger-debugging-pic-pickit3-compatible-para-microcontroladores-detail)

#### Cristal u oscilador (20MHz o 4MHz):
- [sumador.com](https://sumador.com/products/cristales-osciladores?variant=26386993479780)
- [sigmaelectronica.net](https://www.sigmaelectronica.net/producto/xtal-20-mhz/)
- [electronicoscaldas.com](https://bit.ly/33hRSFJ)
- [didacticaselectronicas.com](https://www.didacticaselectronicas.com/index.php/componentes-pasivos/cristales-y-osciladores/cristal-20-mhz-through-hole-cristales-osciladores-resonadores-through-hole-de-20-mhz-detail)
- [vistronica.com](https://www.vistronica.com/componentes-activos/osciladores/cristal-oscilador-de-20-mhz-tht-detail.html)

#### Regulador de voltaje 5V:
- [sumador.com](https://sumador.com/products/regulador-lm7805-5v-1a?_pos=10&_sid=24f8bfe8a&_ss=r)
- [sigmaelectronica.net](https://www.sigmaelectronica.net/producto/l7805cv/)
- [electronicoscaldas.com](https://www.electronicoscaldas.com/es/reguladores-referencias-conversores-de-voltaje/133-regulador-7805.html?search_query=7805&results=5)
- [didacticaselectronicas.com](https://www.didacticaselectronicas.com/index.php/semiconductores/reguladores/reguladores-5v/regulador-de-voltaje-5v-1a-encap-to-220-reguladores-de-voltaje-5v-to220-to-220-detail)
- [vistronica.com](https://www.vistronica.com/componentes-activos/regulador-5v-7805-detail.html)

#### LCD:
- [sumador.com](https://sumador.com/products/display-lcd-2x16-monocromatico-retroiluminacion-verde?_pos=1&_sid=8414435be&_ss=r)
- [sigmaelectronica.net](https://www.sigmaelectronica.net/producto/lcd1602a-azul/)
- [vistronica.com](https://www.vistronica.com/display/lcd-16x2-backlight-azul-detail.html)		

#### Sensor de temperatura:
- [sumador.com](https://sumador.com/products/sensor-de-temperatura-lm35?_pos=1&_sid=368135e32&_ss=r)
- [sigmaelectronica.net](https://www.sigmaelectronica.net/producto/lm35-dz/)
- [electronicoscaldas.com](https://www.electronicoscaldas.com/es/sensores-de-temperatura/194-sensor-de-temperatura-lm35.html?search_query=LM35&results=9)
- [vistronica.com](https://www.vistronica.com/sensores/temperatura/lm35-sensor-de-temperatura-detail.html)		

#### Componentes varios:
- LED
- Cables
- Pulsadores
- Resistencias
- Display 7-segmentos (Simples, o dinámicos)
- Teclado matricial
- Baquelita		
	
#### Herramientas varias:
- Cautín 
- Pinzas
- Protoboard
- Papel termo-transferible o propalcote	
